package com.example.s528772.eventsrus;

/**
 * Created by s528772 on 10/5/2017.
 */

public class Event {
    private String Name;
    private Integer Count;

    public Event(String name, Integer Count) {
        this.Name = name;
        this.Count = 0;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    @Override
    public String toString() {
        return "Event{" +
                "Name='" + Name + '\'' +
                ", Count=" + Count +
                '}';
    }
}
