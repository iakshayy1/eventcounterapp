package com.example.s528772.eventsrus;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class EventAdapter extends ArrayAdapter {

    public EventAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull ArrayList<Event> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView tV1 = (TextView) view.findViewById(R.id.text1);
        TextView tV2 = (TextView) view.findViewById(R.id.text2);
        Event ArrayItem = (Event) getItem(position);
        tV1.setText(ArrayItem.getName());
        tV2.setText(ArrayItem.getCount().toString());
        return view;

    }
}