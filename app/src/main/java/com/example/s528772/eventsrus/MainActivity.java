package com.example.s528772.eventsrus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Event> EventInfo = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ArrayAdapter<Event> adapter = new EventAdapter(this,R.layout.simple_list_item_1,R.id.text1, EventInfo);
        final ListView lV = (ListView) findViewById(R.id.listView);
        lV.setOnItemClickListener(new ListView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Event eventObject=EventInfo.get(position);
                eventObject.setCount(eventObject.getCount()+1);
                Log.d("test","eventsssssssss"+eventObject);
                lV.setAdapter(adapter);
                Toast.makeText(getApplicationContext(),"Event counted",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void newEvent(View v){

        ArrayAdapter<Event> adapter = new EventAdapter(this,R.layout.simple_list_item_1, R.id.text1, EventInfo);
        ListView lV = (ListView) findViewById(R.id.listView);
        EditText eT = (EditText) findViewById(R.id.editText);
        String eventName = eT.getText().toString();
        if(!eventName.isEmpty()) {
            EventInfo.add(new Event(eventName, 0));
            Log.d("test","eventsssssssss"+EventInfo);
            Toast.makeText(getApplicationContext(), "Event added", Toast.LENGTH_SHORT).show();
            lV.setAdapter(adapter);
        }

    }

    public void remove(View v){
        if(EventInfo.size()>0) {
            EventInfo.remove(EventInfo.size() - 1);
            ArrayAdapter<Event> adapter = new EventAdapter(this, R.layout.simple_list_item_1, R.id.text1, EventInfo);
            ListView lV = (ListView) findViewById(R.id.listView);
            lV.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            Toast.makeText(getApplicationContext(), "Removed", Toast.LENGTH_SHORT).show();
        }
        else
        {
            Toast.makeText(getApplicationContext(), "No items", Toast.LENGTH_SHORT).show();
        }

    }

    public void SortByCount(View v){
        Collections.sort(EventInfo, new Comparator<Event>()
        {
            @Override
            public int compare(Event event, Event event1) {

                return Integer.valueOf(event.getCount()).compareTo(event1.getCount());
            }
        });

        for(int i=0;i<=EventInfo.size();i++){
            ArrayAdapter<Event> adapter = new EventAdapter(this, R.layout.simple_list_item_1, R.id.text1, EventInfo);
            ListView lV = (ListView) findViewById(R.id.listView);
            lV.setAdapter(adapter);

        }
    }

    public void SortByName(View v){
        Collections.sort(EventInfo, new Comparator<Event>() {
            @Override
            public int compare(Event event1, Event event2) {
                return event1.getName().compareToIgnoreCase(event2.getName());
            }

        });

        for(int i=0;i<=EventInfo.size();i++){
            ArrayAdapter<Event> adapter = new EventAdapter(this, R.layout.simple_list_item_1, R.id.text1, EventInfo);
            ListView lV = (ListView) findViewById(R.id.listView);
            lV.setAdapter(adapter);
        }
    }

    public void clear(View v){
        EditText eD = (EditText) findViewById(R.id.editText);
        eD.setText("");
    }
}
